#include<stdio.h>

struct student{
    char name[10];
    char subject[10];
    int marks;
} ;

int main(){
    struct student std[5];
    for(int i=0; i<5; i++ ){
        printf("Enter student's first name:");
        scanf("%s",std[i].name);
        printf("Enter subject name:");
        scanf("%s",std[i].subject);
        printf("Enter marks:");
        scanf("%d",&std[i].marks);
    }
    printf("\n\n-------Details-------\n\n");
    for(int i=0; i<5; i++ ){
        printf("Student name: %s\n",std[i].name);
        printf("Subject name: %s\n",std[i].subject);
        printf("Marks: %d\n\n",std[i].marks);
    }

    return 0;
}
